// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

// Destructure importation

import { Button, Row, Col } from 'react-bootstrap';

export default function Banner() {
	return (
		<Row>
			<Col id="bannerCol">
				<h1 id="banner">Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere.</p>
				<Button variant="primary">Enroll now!</Button>
			</Col>
		</Row>
	)
}