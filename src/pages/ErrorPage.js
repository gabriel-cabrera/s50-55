import { Row, Col } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';

export default function NotFoundContent() {

	return (
		<Row>
			<Col>
				<h1>Page Not Found</h1>
				<p>Click here to <Link to="/">home page</Link></p>
			</Col>
		</Row>
	)
}