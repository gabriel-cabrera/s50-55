import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// Import Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <>
    <App/>
  </>
);



// const user = {
//   firstName : 'Mr.',
//   lastName : 'Eminem'
// };

// function formatName(user){

//   return user.firstName + ' ' + user.lastName;
// };

// const element = <h1> Hello, {formatName(user)}</h1>


